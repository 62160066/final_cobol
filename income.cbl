       IDENTIFICATION DIVISION. 
       PROGRAM-ID. income.
       AUTHOR. Mankhong.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT  INPUT-FILE ASSIGN TO "trader6.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT-FILE.
       01  INPUT-BUFFER.
           88 END-OF-INPUT-FILE VALUE HIGH-VALUE .
           05 PID PIC X(2).
           05 T-ID  PIC X(4).
           05 T-INCOME PIC 9(6).

       WORKING-STORAGE SECTION. 
       01 RPT-HEADER.
           05 FILLER  PIC X(13) VALUE "PROVINCE     ".
           05 FILLER  PIC X(13) VALUE " P INCOME    ".
           05 FILLER  PIC X(13) VALUE "  MEMBER    ".
           05 FILLER  PIC X(13) VALUE "MEMBER INCOME".
       01  MAX-T.
           05 MAX-TID PIC X(4).
           05 MAX-T-INCOME   PIC $(1)9(6).
       01  PROVINCE-SUBTOTOAL  PIC 9(7).
       01  P-PROCESS PIC X(2).
       
       01  T-PROCESS PIC X(4).
       01  T-TOTAL PIC 9(6).
       01  MAX-P .
           05 MAX-PID PIC X(2).
           05 MAX-P-INCOME PIC $$$$,$$$,$$9.99.
       01  RPT-LINE.
           05 RPT-PID PIC X(2).
           05 RPT-PIN PIC $$$$,$$$,$$9.99.
           05 RPT-MAX-TID PIC X(4).
           05 RPT-MAX-T-INCOME PIC $(1)9(6).
             
       PROCEDURE DIVISION .
           
       BEGIN.
           OPEN INPUT INPUT-FILE 
           DISPLAY RPT-HEADER 
           PERFORM READ-LINE
           PERFORM PROCESS-PROVINCE UNTIL END-OF-INPUT-FILE 
           
           
           GOBACK .
       READ-LINE.
           READ INPUT-FILE 
              AT END SET END-OF-INPUT-FILE TO TRUE 
           CLOSE INPUT-FILE
           .
      
       PROCESS-PROVINCE.
           MOVE PID TO P-PROCESS 
           MOVE ZERO TO PROVINCE-SUBTOTOAL
           MOVE P-PROCESS TO RPT-PID 
           PERFORM  PROCESS-T UNTIL PID NOT = P-PROCESS           
           MOVE PROVINCE-SUBTOTOAL TO RPT-PIN 
           DISPLAY RPT-LINE
           .
       PROCESS-T.
           MOVE T-ID TO T-PROCESS 
           MOVE ZERO TO T-TOTAL .
           PERFORM PROCESS-LINE UNTIL T-ID NOT = T-PROCESS
                    OR PID NOT = P-PROCESS
           MOVE MAX-T-INCOME  TO RPT-MAX-T-INCOME
           MOVE MAX-TID TO RPT-MAX-TID 
           DISPLAY T-TOTAL 
           .
       PROCESS-LINE.
           MOVE T-INCOME TO T-TOTAL
           ADD T-TOTAL TO PROVINCE-SUBTOTOAL
           IF T-TOTAL > MAX-T-INCOME THEN
               MOVE T-TOTAL  TO MAX-T-INCOME
               MOVE T-PROCESS TO MAX-TID           
           .
           

